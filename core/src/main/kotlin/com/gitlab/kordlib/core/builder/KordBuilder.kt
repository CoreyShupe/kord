package com.gitlab.kordlib.core.builder

@DslMarker
@Target(AnnotationTarget.CLASS)
annotation class KordBuilder